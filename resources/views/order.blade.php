@extends('layouts/master') @section('content')
<section class="row-fluid">
	<!-- Start Main Content -->
	<section class="span12 cart-holder">
		<div class="heading-bar">
			<h2>Your Orders</h2>
			<span class="h-line"></span>
		</div>

		<table width="90%" align=center>
			<tr>
				<th align=center>Order #</th>
				<th align=center>Date</th>
				<th align=right>Shipping</th>
				<th align=right>Subtotal</th>
				<th align=right>Grand Total</th>
			</tr>
			@foreach ($orders as $order)
			<tr>
				<td align=center><a href="{{url("order/$order->id") }}">{{$order->id}}</a></td>
				<td align=center>{{date('m/d/Y h:i A', strtotime($order->created_at))}}</td>
				<td align=right>${{number_format($order->shipping,2)}}</td>
				<td align=right>${{number_format($order->total,2)}}</td>
				<td align=right>${{number_format($order->shipping +
					$order->total,2)}}</td>
			</tr>
			@endforeach
		</table>

	</section>
</section>

@endsection
