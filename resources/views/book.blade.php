@extends('layouts/master')

@section('content')
<section class="row-fluid">
    	<div class="heading-bar">
        	<h2>Book Detail</h2>
            <span class="h-line"></span>
        </div>
        <!-- Start Main Content -->
        <section class="span12 first">
        
            <!-- Strat Book Detail Section -->
            <section class="b-detail-holder">
            	<article class="title-holder">
                	<div class="span12">
                    	<h4><strong>{{$book->title}}</strong> by 
                    	@foreach ($authors as $author)
                    		{{ $author->name }}
                    	@endforeach
                    	</h4>
                    </div>
                </article>
                <div class="book-i-caption">
                <!-- Strat Book Image Section -->
                	<div class="span6 b-img-holder">
                        <span class='zoom' id='ex1'> <img src='{{$book['cover_url']}}' height="219" width="300" id='jack' alt=''/></span>
                    </div>
                <!-- Strat Book Image Section -->
                
                <!-- Strat Book Overview Section -->    
                    <div class="span6">
                    	<strong class="title">Quick Overview</strong>
                    	{!! strip_tags(str_limit($book['description'], $limit=1000, $end='...') )!!}
                   		
                        <div class="comm-nav">
                   			<strong class="title2">Quantity</strong>
                            <ul>
                            	<li><input name="" type="text" value="1" class='quantity'/></li>
                                <li class="b-price">${{ number_format($book->price, 2)}}</li>
                                <li><a href="{{url('cart/add', $parameters=['id' => $book->id])}}" class="more-btn">Add to Cart</a></li>
                            </ul>
                        </div>
                   </div>
                <!-- End Book Overview Section -->
                </div>
                
                <!-- Start Book Summary Section -->
                	<div class="tabbable">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#pane1" data-toggle="tab">Book Summary</a></li>
                        
                      </ul>
                      <div class="tab-content">
                        <div id="pane1" class="tab-pane active">
                            <p><strong>ISBN: </strong>{{$book->isbn}}</p>
                            <p><strong>Publisher: </strong>{{$book->publisher}}</p>
                            <p><strong>Course Code: </strong>{{$book->courses()->first()->code}}</p>
                            <p><strong>Course Title: </strong>{{$book->courses()->first()->title}}</p>
                            
                          {!! $book['description']!!}
                        </div>
                      </div><!-- /.tab-content -->
                    </div><!-- /.tabbable -->
                <!-- End Book Summary Section -->
            
           
            </section>
            <!-- Strat Book Detail Section -->
        </section>
        <!-- End Main Content -->
        
    </section>
@endsection

@section('script')

    $(function() {
        $('.more-btn').click(function(e) {
            var book_id = {{$book->id}};
            var quantity = $('.quantity').val();
            var token = '{{{ csrf_token() }}}';

            $.ajax({
              type: "POST",
              url: '/cart/add?t='+Math.random(),
              data: { 'book_id' : book_id, 'quantity' : quantity, '_token' : token},
              complete: function() {
                var url = "/cart";    
                $(location).attr('href',url);
              },
            });

            e.preventDefault();

        });
    });

@endsection