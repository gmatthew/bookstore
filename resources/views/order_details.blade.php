@extends('layouts/master') @section('content')
<section class="row-fluid">
	<!-- Start Main Content -->
	<section class="span12 cart-holder">
		<div class="heading-bar">
			<h2>Order #{{$order->id}}</h2>
			<span class="h-line"></span>
		</div>
		<table width="90%" border="0" cellpadding="10">
			<tr class="heading-bar-table">
			    <th with="5%"></th>
				<th width="42%" align="left">Book Title</th>
				<th width="18%">Price</th>
				<th width="19%">Quantity</th>
				<th width="16%" align=right>Subtotal</th>
			</tr>
			@foreach ($books as $book)
			<tr>
			    <td with="5%"><img src="{{$book->cover_url }}" width="100%" /></td>
				<td width="42%" align="left"><a href="{{ url('book/'.$book->id) }}">{{$book->title}}</a></td>
				<td width="18%" align=center>${{number_format($book->price,2)}}</td>
				<td width="19%" align=center>{{$book->quantity}}</td>
				<td width="16%" class='subtotal' align=right>${{number_format($book->quantity
					* $book->price,2) }}</td>
			</tr>
			@endforeach

			<tr>
				<td colspan="4" align="right">
					<p>Subtotal</p>
					<p>Shipping & Handing</p>
					<p>Grand Total</p>
				</td>
				<td class='subtotal' align=right>
					<p>${{number_format($order->total, 2)}}</p>
					<p>${{number_format($order->shipping,2)}}</p>
					<p>${{number_format($order->shipping + $order->total, 2)}}</p>
				</td>
			</tr>
		</table>

	</section>
</section>

@endsection
