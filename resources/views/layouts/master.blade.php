<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Comet Bookstore | The University Bookstore!</title>
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width">
<!-- Css Files Start -->
{!! HTML::style('css/style.css') !!}
<!-- All css -->
{!!HTML::style('css/bs.css') !!}
<!-- Bootstrap Css -->
{!! HTML::style('css/main-slider.css') !!}
<!-- Main Slider Css -->
<!--[if lte IE 10]><link rel="stylesheet" type="text/css" href="css/customIE.css" /><![endif]-->
{!! HTML::style('css/font-awesome.css') !!}
<!-- Font Awesome Css -->
{!! HTML::style('css/font-awesome-ie7.css') !!}
<!-- Booklet Css -->
{!! HTML::style('css/jquery.booklet.latest.css') !!}
<noscript>
{!! HTML::style('css/noJS.css') !!}
</noscript>
<!-- Css Files End -->
</head><body>
<!-- Start Main Wrapper -->
<div class="wrapper"> 
  <!-- Start Main Header --> 
  <!-- Start Top Nav Bar -->
  <section class="top-nav-bar">
    <section class="container-fluid container">
      <section class="row-fluid">
        <section class="span6">
          <ul class="top-nav">
            <li><a href="{{ url('/')}}" class="active">Home</a></li>
            <li><a href="{{ url('/contact') }}">Contact Us</a></li>
          </ul>
        </section>
        <section class="span6 e-commerce-list">
          <ul class="user-info">
            <li>
            @if (isset($user->first_name)) 
                Hi <a href="#">{{$user->first_name}}!</a> | <a href="{{url('auth/logout') }}">Logout</a></li>
            @else
                <a href="{{url('auth/login') }}">Login</a> | <a href="{{url('auth/register') }}">Register</a></li>
            @endif
            
          </ul>
          <div class="c-btn"> <a href="{{ url('/cart')}}" class="cart-btn">Cart</a>
            <div class="btn-group">
              <button data-toggle="dropdown" class="btn btn-mini"><span class="cart-count">0</span> item(s) - $<span class="cart-total">0.00</span></button>
              
            </div>
          </div>
        </section>
      </section>
    </section>
  </section>
  <!-- End Top Nav Bar -->
  <header id="main-header">
    <section class="container-fluid container">
      <section class="row-fluid">
        <section class="span4">
          <h1 id="logo"> <a href="{{ url('/')}}"><img src="{!! asset('images/logo.png') !!}" style="max-height:100px" /></a> </h1>
        </section>
        <section class="span8">
          @if (isset($user->first_name)) 
          <ul class="top-nav2">
            <li><a href="{{ url('/account') }}">My Account</a></li>
            <li><a href="{{ url('/order') }}">Orders</a></li>
            <li><a href="{{ url('/cart') }}">My Cart</a></li>
            <li><a href="{{ url('/checkout') }}">Checkout</a></li>
          </ul>
          @endif
          <div class="search-bar">
            <form method="get" action="{{ url('/search') }}">
                <input name="q" type="text" placeholder="Search By ISBN, Title, Author or Course" />
                <input type="submit" value="Serach" />
            </form>
          </div>
        </section>
      </section>
    </section>
    <!-- Start Main Nav Bar -->
    @if(isset($courses))
    <nav id="nav">
      <div class="navbar navbar-inverse">
        <div class="navbar-inner">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <div class="nav-collapse collapse">
            <ul class="nav">
            @foreach($courses as $course)
                <li> <a href="{{url('/search?q='.$course->code) }}">{{$course->title}}</a> </li>
            @endforeach
            </ul>
          </div>
          <!--/.nav-collapse --> 
        </div>
        <!-- /.navbar-inner --> 
      </div>
      <!-- /.navbar --> 
    </nav>
    @endif
    <!-- End Main Nav Bar --> 
  </header>
  <!-- End Main Header --> 

  <!-- Start Main Content Holder -->
  <section id="content-holder" class="container-fluid container">
      @yield('content')
  </section>
  <!-- End Main Content Holder --> 

  <!-- Start Footer Top 1 -->
  <section class="container-fluid footer-top1">
    <section class="container">
      <section class="row-fluid">
        <figure class="span3">
          <h4>Location</h4>
          <p>800 W Campbell St, Richardson, TX 75080</p>
          <span>
          <ul class="phon-list">
            <li>(972) 438-BOOK</li>
            <li>info@cometbookstore.com</li>
          </ul></figure>
        <figure class="span3">
          <h4>Opening Time</h4>
          <p>Monday-Friday ______8.00 to 18.00</p>
          <p>Saturday ____________ 9.00 to 18.00</p>
          <p>Sunday _____________10.00 to 16.00</p>
        </figure>
      </section>
    </section>
  </section>
  <!-- End Footer Top 1 --> 
  <!-- Start Footer Top 2 -->
  <section class="container-fluid footer-top2">
    <section class="social-ico-bar">
      <section class="container">
        <section class="row-fluid">
          <ul class="footer2-link">
            <li><a href="{{ url('/contact') }}">Contact Us</a></li>
          </ul>
        </section>
      </section>
    </section>
  </section>
  <!-- End Footer Top 2 --> 
  <!-- Start Main Footer -->
  <footer id="main-footer">
    <section class="social-ico-bar">
      <section class="container">
        <section class="row-fluid">
          <article class="span6">
            <p>© {{ date('Y') }}  Comet Bookstore</p>
          </article>
          <article class="span6 copy-right">
            <p>Developed by Team Purple</p>
          </article>
        </section>
      </section>
    </section>
  </footer>
  <!-- End Main Footer --> 
</div>
<!-- End Main Wrapper --> 
<!-- JS Files Start --> 
<script type="text/javascript" src="{!! asset('js/lib.js') !!}"></script><!-- lib Js --> 
<script type="text/javascript" src="{!! asset('js/modernizr.js') !!}"></script><!-- Modernizr --> 
<script type="text/javascript" src="{!! asset('js/easing.js') !!}"></script><!-- Easing js --> 
<script type="text/javascript" src="{!! asset('js/bs.js') !!}"></script><!-- Bootstrap --> 
<script type="text/javascript" src="{!! asset('js/bxslider.js') !!}"></script><!-- BX Slider --> 

<script src="{!! asset('js/range-slider.js') !!}"></script><!-- Range Slider --> 
<script src="{!! asset('js/jquery.zoom.js') !!}"></script><!-- Zoom Effect --> 
<script type="text/javascript" src="{!! asset('js/bookblock.js') !!}"></script><!-- Flip Slider --> 
<script type="text/javascript" src="{!! asset('js/custom.js') !!}"></script><!-- Custom js --> 
<script type="text/javascript" src="{!! asset('js/social.js') !!}"></script><!-- Social Icons --> 
<script src="{!! asset('js/jquery.booklet.latest.js') !!}" type="text/javascript"></script><!-- Booklet Js --> 
<script type="text/javascript">
      $(function () {   
          $("#mybook").booklet({
        width:'100%',
        height:430,
        auto: true,
        //speed: 250,
      });
      });
    </script> 
<!-- JS Files End -->
<noscript>
<style>
  #socialicons>a span { top: 0px; left: -100%; -webkit-transition: all 0.3s ease; -moz-transition: all 0.3s ease-in-out; -o-transition: all 0.3s ease-in-out; -ms-transition: all 0.3s ease-in-out; transition: all 0.3s  ease-in-out;}
  #socialicons>ahover div{left: 0px;}
  </style>
</noscript>
<script type="text/javascript">
  /* <![CDATA[ */
  $(document).ready(function() {
  $('.social_active').hoverdir( {} );
})

   $(function() {
        $.ajax({
          type: "GET",
          url: '/cart/summary?t='+Math.random(),
          success: function(data) {
        	  $('.cart-count').text(data.count);
        	  $('.cart-total').text(data.total_price);
          },
        });
    });
  
  @yield('script');
/* ]]> */
</script>
</body>
</html>
