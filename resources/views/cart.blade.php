@extends('layouts/master')

@section('content')
<section class="row-fluid">
        <!-- Start Main Content -->
        <section class="span12 cart-holder">
            <div class="heading-bar">
            	<h2>SHOPPING CART</h2>
                <span class="h-line"></span>
            	<a href="{{url('checkout')}}" class="more-btn">proceed to checkout</a>
            </div>

            @if (count($items) > 0)

            <div class="cart-table-holder">
            	<table width="100%" border="0" cellpadding="10">
                  <tr>
                    <th width="14%">&nbsp;</th>
                    <th width="43%" align="left">Book Title</th>
                    <th width="6%"></th>
                    <th width="10%">Unit Price</th>
                    <th width="10%">Quantity</th>
                    <th width="12%">Subtotal</th>
                    <th width="5%">&nbsp;</th>
                  </tr>
                  @foreach ($items as $item)
                  <tr bgcolor="#FFFFFF" class=" product-detail">
                    <td valign="top"><a href="{{url('book/'.$item->book->id)}}"><img src="{{$item->book->cover_url }}" /></a></td>
                    <td valign="top">{{$item->book->title }}</td>
                    <td align="center" valign="top">&nbsp;</td>
                    <td align="center" valign="top">${{number_format($item->book->price,2) }}</td>
                    <td align="center" valign="top"><input name="" type="text" data-book-id="{{$item->book->id}}" data-initial-value="{{$item->quantity}}" value="{{$item->quantity }}" class='quantity' /></td>
                    <td align="center" valign="top">${{number_format(floatval($item->book->price * $item->quantity), 2)}}</td>
                    <td align="center" valign="top"><a href="#" data-book-id="{{$item->book->id}}" class="delete-book"> <i class="icon-trash"></i></a></td>
                  </tr>
                  @endforeach
                </table>

            </div>
            
            <figure class="span12 price-total" style="margin-left:0px">
            	<div class="cart-option-box">
                    <table width="100%" border="0" class="total-payment">
                      
                      <tr>
                        <td align="right"><strong class="large-f">TOTAL: </strong></td>
                        <td align="left" width="15%"><strong class="large-f">${{ number_format($total, 2)}}</strong></td>
                      </tr>
                  </table>
                  <hr />
                   <p align="right"> <a href="{{url('checkout')}}" class="more-btn">proceed to checkout</a></p>
                </div>
            </figure>
            @else
            	<div class="cart-table-holder">
            		<p>Your Cart is Empty!</p>
            	</div>

            @endif
        </section>
        <!-- End Main Content -->
    </section>
@endsection

@section('script')

$(function() {

	var token = '{{{ csrf_token() }}}';

	$('.delete-book').each(function(i,e) {

		$(this).click(function() {
			var book_id = $(this).data('book-id');
	        var session_id = '{{$session_id}}';
	       

			$.ajax({
	              type: "POST",
	              url: '/cart/delete?t='+Math.random(),
	              data: { 'book_id' : book_id, 'session_id' : session_id, '_token' : token},
	              success: function() {
	                var url = "/cart";    
	                $(location).attr('href',url);
	              },
	            });

        })
	});


	$('.quantity').each(function(i,e){
		$(this).blur(function() {

			var quantity = $(this).val();
			var init_quantity = $(this).data('initial-value');
			var book_id = $(this).data('book-id');

			if (quantity != init_quantity) {
				$.ajax({
	              type: "POST",
	              url: '/cart/update?t='+Math.random(),
	              data: { 'book_id' : book_id, 'quantity': quantity, '_token' : token},
	              success: function() {
	                var url = "/cart";    
	                $(location).attr('href',url);
	              },
	            });
			}
		});
	});
});

@endsection