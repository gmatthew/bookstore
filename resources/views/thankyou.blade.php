@extends('layouts/master') @section('content')
<section class="row-fluid">
	<!-- Start Main Content -->
	<section class="span12 cart-holder">
		<div class="heading-bar">
			<h2>Order #{{$order_id}}</h2>
			<span class="h-line"></span>
		</div>
		<div class="cart-table-holder">
			<p>We appreciate your business. Your order will be shipping
				shortly...</p>
		</div>
	</section>
</section>

@endsection
