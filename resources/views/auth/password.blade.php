@extends('layouts/master') @section('content')
<section class="row-fluid">
	<div class="heading-bar">
		<h2>Reset Password</h2>
		<span class="h-line"></span>
	</div>
	<section class="span12 first">
		<div class="container-fluid">
			<div>
				<div class="col-md-8 col-md-offset-2">
					<div class="panel panel-default">

						<div class="panel-body">
							@if (session('status'))
							<div class="alert alert-success">{{ session('status') }}</div>
							@endif @if (count($errors) > 0)
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your
								input.<br> <br>
								<ul>
									@foreach ($errors->all() as $error)
									<li>{{ $error }}</li> @endforeach
								</ul>
							</div>
							@endif

							<form class="form-horizontal" role="form" method="POST"
								action="/password/email">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">

								<div class="form-group">
									<label class="col-md-4 control-label">E-Mail Address</label>
									<div class="col-md-6">
										<input type="email" class="form-control" name="email"
											value="{{ old('email') }}">
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-6 col-md-offset-4">
										<button type="submit" class="btn btn-primary">Send Password
											Reset Link</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
@endsection
