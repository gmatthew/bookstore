@extends('layouts/master')

@section('content')
<section class="row-fluid">
    	<div class="heading-bar">
        	<h2>Search Results</h2>
            <span class="h-line"></span>
        </div>
        <!-- Start Main Content -->
        <section class="span12 first">
        	
            <section class="grid-holder features-books">
                <?php $first = ''; ?>
                @for($i=0; $i < count($results); $i++)
               <?php $book = $results[$i]; ?>
            	<figure class="span2 slide {{$first}}">
                	<a href="{{url('book/'.$book->id) }}"><img src="{{$book->cover_url}}" alt="" class="pro-img" height="100"/></a>
                    <span class="title"><a href="{{url('book/'.$book->id) }}">{{$book->title}}</a></span>
                    <div class="cart-price">
                        <a class="cart-btn2" href="#" data-book-id="{{$book->id}}">Add to Cart</a>
                        <span class="price">${{ number_format($book->price,2) }}</span>
              		</div>
                </figure>
                <?php if (( $i > 0 ) && ((($i+1)% 6) == 0)) :?>
                    <hr/>
                <?php endif; ?>
                
                <?php $first = ((($i+1) % 6) == 0) ? 'first' : '1'; ?>
                @endfor
                
                @if(count($results) == 0) 
                    No Results Found!
                @endif
            </section>
            
            
        </section>
        <!-- End Main Content -->
      
    </section>
@endsection

@section('script')

    $(function() {
        $('.cart-btn2').click(function(e) {
            var book_id = $(this).data('book-id');
            var quantity = 1;
            var token = '{{{ csrf_token() }}}';

            $.ajax({
              type: "POST",
              url: '/cart/add?t='+Math.random(),
              data: { 'book_id' : book_id, 'quantity' : quantity, '_token' : token},
              complete: function() {
                var url = "/cart";    
                $(location).attr('href',url);
              },
            });

            e.preventDefault();

        });
    });
@endsection