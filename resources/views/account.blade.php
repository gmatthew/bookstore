@extends('layouts/master')


@section('content')

 <form class="form-horizontal" role="form" method="POST" action="{{url('account/update') }}">
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
<section class="row-fluid">
    @if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
    <div class="heading-bar">
        <h2>Account</h2>
        <span class="h-line"></span> </div>
      <!-- Start Main Content -->
      <section class="checkout-holder">
        <section class="span12 first">
          <!-- Start Accordian Section -->
          <div class="accordion" id="accordion2">
            
            <div class="accordion-group">
              <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Personal Information</a> </div>
              <div id="collapseOne" class="accordion-body collapse in">
                <div class="accordion-inner">
                	
                   
                        <ul class="billing-form">
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputFirstname">First Name <sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputFirstname" placeholder="" name="first_name" value="{{ $user->first_name }}">
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label" for="inputLastname">Last Name<sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputLastname" placeholder="" name="last_name" value="{{ $user->last_name }}">
                                </div>
                              </div>
                              
                            </li>
                           
                        </ul>
                   
                </div>
              </div>
            </div>
            
            <div class="accordion-group">
              <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"> Billing Information </a> </div>
              <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                	
                   
                        <ul class="billing-form">
                            
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputAddress">Address<sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputAddress" placeholder="" class="address-field" name="billing_address" value="{{ $billing_address->street_1 }}">
                                </div>
                              </div>
                            </li>
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputCity">City <sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputCity" placeholder="" name="billing_city" value="{{ $billing_address->city }}">
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label" for="inputState/Province">State/Province<sup>*</sup></label>
                                <div class="controls">
                                  <select name="billing_state" size="1">
                                    <option selected value="">State...</option>
                                    @foreach($states as $key => $state) 
                                    <option value="{{$key}}"
                                    @if($billing_address->state == $key) 
                                           selected
                                        @endif
                                    >{{$state}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </li>
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputZip">Zip/Postal Code <sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputZip" placeholder="" name="billing_zip" value="{{ $billing_address->zip_code }}">
                                </div>
                              </div>
                             
                            </li>
                            <li>   
                              
                               <strong class="green-t">* Required Fields</strong>
                            </li>
                        	
                        </ul>
                   
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree"> Shipping Information </a> </div>
              <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner"> 
                        <ul class="billing-form">
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputAddress">Address<sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputAddress" placeholder="" class="address-field" name="shipping_address" value="{{ $shipping_address->street_1 }}">
                                </div>
                              </div>
                            </li>
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputCity">City <sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputCity" placeholder="" name="shipping_city" value="{{ $shipping_address->city }}">
                                </div>
                              </div>
                              <div class="control-group">
                                <label class="control-label" for="inputState/Province">State/Province<sup>*</sup></label>
                                <div class="controls">
                                  <select size="1" name="shipping_state">
                                    <option selected value="">State...</option>
                                    @foreach($states as $key => $state) 
                                    <option value="{{$key}}" 
                                         @if($shipping_address->state == $key) 
                                           selected
                                        @endif
                                    >{{$state}}</option>
                                    @endforeach
                                    
                                    
                                  </select>
                                </div>
                              </div>
                            </li>
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputZip">Zip/Postal Code <sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputZip" placeholder="" name="shipping_zip" value="{{ $shipping_address->zip_code }}">
                                </div>
                              </div>
                            </li>
                            <li>
                             <strong class="green-t">* Required Fields</strong>
                              </li>
                        	
                        </ul>
                   
                </div>
              </div>
            </div>
            <div class="accordion-group">
              <div class="accordion-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFifth"> Credit Card Information </a> </div>
              <div id="collapseFifth" class="accordion-body collapse">
                <div class="accordion-inner">
                	
                        <ul class="billing-form">
                            <li>   
                            	
                              <div class="control-group">
                                <label class="control-label" for="inputCardname">Name on Card<sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputCardname" placeholder="" name="cc_name" value="{{ $credit_card->name }}" >
                                </div>
                              </div>
                            </li>
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputCardtype">Credit Card Type <sup>*</sup></label>
                                <div class="controls">
                                  <select name="cc_type" size="1">
                                    @foreach ($credit_card_types as $type) 
                                        <option  value="{{$type->id}}" 
                                        @if($credit_card->type_id == $type->id) 
                                           selected
                                        @endif>{{$type->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </li>                            
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputCreditcardnum">Credit Card Number  <sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputCreditcardnum" placeholder="" name="cc_number" value="{{ $credit_card->number }}" maxlength=16>
                                </div>
                              </div>
                              
                            </li>
                            <li>
                            	<div class="control-group w-extra">
                                <label class="control-label" for="inputExpirationdate">Expiration Date<sup>*</sup></label>
                                <div class="controls">
                                  <select name="cc_month"size="1" class="month-list">
                                   @for($i=1; $i <=12; $i++)
                                    <option value="{{$i}}" 
                                         @if($credit_card->expiration_month == $i) 
                                           selected
                                        @endif
                                        ><?php echo date('M', strtotime("2015-$i-1")) ?></option>
                                   @endfor 
                                  </select>
                                  <select name="cc_year"size="1" class="year-list">
                                    @for($i=0; $i<10; $i++)
                                    <?php $year = date('Y',strtotime( (date('Y') +$i).'-1-1' ))?>
                                    <option value="{{$year}}" 
                                        @if($credit_card->expiration_year == $year) 
                                           selected
                                        @endif
                                    >{{$year}}</option>
                                    @endfor
                                  </select>
                                </div>
                              </div>
                            </li>
                            <li>   
                              <div class="control-group">
                                <label class="control-label" for="inputCardVerification"> Card Verification Number <sup>*</sup></label>
                                <div class="controls">
                                  <input type="text" id="inputCardVerification" placeholder="" name="cc_verification" maxlength="4" value="{{ $credit_card->security_code }}">
                                  <strong class="green-t">* Required Fields</strong>
                                </div>
                              </div>
                            </li>
                        	
                        </ul>
                    
                </div>
              </div>
            </div>
    
          </div>
          
          <!-- End Accordian Section -->
        </section>
        </section>
        </section>
        <input type="submit" value="Save" />
       </form>
   
@endsection