@extends('layouts/master')

@section('content')

<section class="row-fluid">
      <section class="book-box">
        <div class="book-outer">
          <div id="mybook">
            @foreach ($top_books as $book)
            <div>
              <div class="left-page">
                <div class="frame"><a href="{{url('book/'.$book->id)}}"><img src="{{$book['cover_url']}}" height="338" alt="img"></a></div>
                <div class="bottom">
                  <div class="cart-price"> <span class="cart" data-book-id="{{$book->id}}">&nbsp;</span> <strong class="price">${{number_format($book['price'],2)}}</strong> </div>
                </div>
              </div>
            </div>
            <div>
              <div class="right-page">
                <div class="text">
                  <h1>{{str_limit($book['title'], $limit=200, $end='...')}}</h1>
                  <strong class="name">by {{ $book->authors()->first()->name }}</strong>
                  
                  <a href="#" class="btn-shop" data-book-id="{{$book['id']}}">ADD TO CART</a> </div>
                <div class="bottom">
                  <div class="text">
                    <div class="inner">
                    
                      <a href="{{url('book/'.$book->id)}}" class="readmore">Read More</a> </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </section>
      <section class="span12 wellcome-msg m-bottom first">
        <h2>WELCOME TO Comet Bookstore!</h2>
        <p>Offering a books for all your campus courses at the low prices! Your Campus Store!</p>
      </section>
    </section>
    <section class="row-fluid ">
      @foreach ($onsale as $book)
      <figure class="span4 s-product">
        <div class="s-product-img"><a href="{{url('book/'.$book->id)}}"><img src="{{$book['cover_url']}}" alt="Image02"/></a></div>
        <article class="s-product-det">
          <h3><a href="{{url('book/'.$book->id)}}">{{$book['title']}}</a></h3>
          <div class="cart-price"> <a href="#" class="cart-btn2" data-book-id="{{$book->id}}">Add to Cart</a> <span class="price">${{number_format($book['price'],2)}}</span> </div>
          <span class="sale-icon">Sale</span> </article>
      </figure>
      @endforeach
    </section>
    <!-- Start BX Slider holder -->
    <section class="row-fluid features-books">
      <section class="span12 m-bottom">
        <div class="heading-bar">
          <h2>Featured Books</h2>
          <span class="h-line"></span> </div>
        <div class="slider1">
          @foreach ($featured as $book)
          <div class="slide"> <a href="{!! url('book/'.$book['id']) !!}"><img src="{{$book['cover_url']}}" alt="" class="pro-img"/></a> <span class="title"><a href="{!! url('book/'.$book['id']) !!}">{{str_limit($book['title'], $limit=40, $end='...') }}</a></span> 
            <div class="cart-price"> <a class="cart-btn2" data-book-id="{{$book->id}}" href="#">Add to Cart</a> <span class="price">${{number_format($book['price'],2)}}</span> </div>
          </div>
          @endforeach
        </div>
      </section>
    </section>
    <!-- End BX Slider holder --> 
    
@endsection

@section('script')

    $(function() {
        $('.cart-btn2, .btn-shop, .cart').click(function(e) {
            var book_id = $(this).data('book-id');
            var quantity = 1;
            var token = '{{{ csrf_token() }}}';

            $.ajax({
              type: "POST",
              url: '/cart/add?t='+Math.random(),
              data: { 'book_id' : book_id, 'quantity' : quantity, '_token' : token},
              complete: function() {
                var url = "/cart";    
                $(location).attr('href',url);
              },
            });

            e.preventDefault();

        });
    });
@endsection
