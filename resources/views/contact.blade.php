@extends('layouts/master') @section('content')
<section class="row-fluid">
	<div class="heading-bar">
		<h2>Contact Us</h2>
		<span class="h-line"></span>
	</div>
	<!-- Start Main Content -->
	<section class="span12 first">

		<!-- End Ad Slider Section -->

		<!-- Start Map Section -->
		<section class="map-holder">
			<iframe width="100%" height="350" frameborder="0" scrolling="no"
				marginheight="0" marginwidth="0"
				src="http://maps.google.mu/?ie=UTF8&amp;ll=32.978311,-96.748454&amp;spn=0.093419,0.169086&amp;t=m&amp;z=13&amp;output=embed"></iframe>
		</section>
		<!-- Start Map Section -->

		<div class="span6">
			<strong>Contact Info</strong>
			<p>
				Comet Bookstore,<br /> 800 W Campbell St, <br> Richardson, TX.
			
			</p>
			<p>
				Phone: (972) 438-BOOK <br />
				<br />Email: <a href="#">info@cometbookstore.com</a>
			</p>

		</div>

	</section>
	<!-- End Main Content -->

</section>
@endsection
