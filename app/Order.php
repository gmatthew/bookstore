<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'Order';

    public function customer ()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }

    public function books ()
    {
        return Order::where('Order.id', $this->id)->join('Order_Book', 'Order.id', '=', 'Order_Book.order_id')
            ->join('Book', 'Order_Book.book_id', '=', 'Book.id')
            ->select('*')
            ->get();
    }

    public function saveWithItems ($items)
    {
        $saveCount = 0;
        if ($this->save()) {
            
            foreach ($items as $item) {
                $item_relation = new Order_Book();
                $item_relation->book_id = $item->book->id;
                $item_relation->order_id = $this->id;
                $item_relation->quantity = $item->quantity;
                
                // Save the item to order
                if ($item_relation->save()) {
                    $saveCount ++;
                }
            }
        }
        
        return $saveCount;
    }
}
