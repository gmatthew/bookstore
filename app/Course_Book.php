<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Course_Book extends Model {

	protected $table = 'Course_Book';
	
	protected $fillable = ['book_id', 'course_id'];

}
