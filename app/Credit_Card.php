<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Credit_Card extends Model
{

    protected $table = 'Credit_Card';

    protected $fillable = [
            'customer_id',
            'type_id',
            'name',
            'number',
            'expiration_year',
            'expiration_month',
            'security_code'
    ];
}
