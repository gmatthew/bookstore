<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Book_Author extends Model {

	protected $table = 'Book_Author';

	protected $fillable = ['book_id','author_id'];
}
