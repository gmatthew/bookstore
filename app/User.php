<?php
namespace App;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'first_name',
            'last_name',
            'email',
            'password',
            'shipping_address_id',
            'billing_address_id',
            'credit_card_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
            'password',
            'remember_token'
    ];

    public function orders() {
        return $this->hasMany('App\Order', 'customer_id', 'id');
    }
    
    public function billingAddress ()
    {
        return $this->hasOne('App\Address', 'id', 'billing_address_id');
    }

    public function shippingAddress ()
    {
        return $this->hasOne('App\Address', 'id', 'shipping_address_id');
    }

    public function creditCard ()
    {
        return $this->hasOne('App\Credit_Card', 'id', 'credit_card_id');
    }

    public function setBillingAddress ($data)
    {
        $data['address_type_id'] = 2;
        $data['customer_id'] = $this->id;
        $address = Address::create($data);
        $this->billing_address_id = $address->id;
        $this->save();
    }

    public function setShippingAddress ($data)
    {
        $data['address_type_id'] = 1;
        $data['customer_id'] = $this->id;
        $address = Address::create($data);
        $this->shipping_address_id = $address->id;
        $this->save();
    }

    public function setCreditCard ($data)
    {
        $data['customer_id'] = $this->id;
        $cc = Credit_Card::create($data);
        $this->credit_card_id = $cc->id;
        $this->save();
    }
}
