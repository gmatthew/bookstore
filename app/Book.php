<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {

	protected $table = 'Book';

	protected $fillable = ['isbn', 'title', 'publisher', 'price', 'description', 'cover_url', 'on_sale'];
	
	public function authors() {

		return $this->belongsToMany('App\Author', 'Book_Author', 'book_id', 'author_id');
	}
	
	public function courses() {
	    return $this->belongsToMany('App\Course', 'Course_Book', 'book_id', 'course_id');
	}
	
}
