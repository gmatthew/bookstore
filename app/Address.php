<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $table = 'Address';

    protected $fillable = [
            'customer_id',
            'address_type_id',
            'street_1',
            'street_2',
            'city',
            'state',
            'zip_code',
            'country'
    ];
}
