<?php namespace App\Http\Controllers;

use App;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$random_price = mt_rand(0,200);
		
		$top_books = App\Book::where('price', '>', $random_price)->take(50)->get()->random(10);
		$featured = App\Book::where('price', '<', $random_price)->take(50)->get()->random(10);
		$onsale = App\Book::where('on_sale', '=', 1)->take(5)->get()->random(3);

		return view('home', ['featured' => $featured, 'onsale' => $onsale, 'top_books' => $top_books]);
	}

}
