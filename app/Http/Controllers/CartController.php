<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App;

class CartController extends Controller {

	public function add(Request $request) {

		$book_id = $request->input('book_id');
		$quantity = $request->input('quantity');

		$cart_item = new App\CartItem();
		$cart_item->session_id = $this->session_id;
		$cart_item->book_id = $book_id;
		$cart_item->quantity = $quantity;
		$cart_item->save();
		

	}

	public function delete(Request $request) {
		$book_id = $request->input('book_id');
		App\CartItem::where('session_id', 'like', $this->session_id)->where('book_id', '=', $book_id)->delete();
	}

	public function update(Request $request) {
		$book_id = $request->input('book_id');
		$quantity = $request->input('quantity');

		$book = App\CartItem::where('session_id', 'like', $this->session_id)->where('book_id', '=', $book_id)->first();

		if ($book) {
			if ($quantity > 0) {
				$book->quantity = $quantity;
				$book->save();
			}
			else {
				$book->delete();
			}
		}

	}
	
	
	public function summary() {
	    list($cart_items, $total_price) = $this->getCartData();
	    
	    return response()->json([ 'count' => count($cart_items), 'total_price' => number_format($total_price,2)]);
	}
	
	
	public function show() {
	    
		list($cart_items, $total) = $this->getCartData();

		return view('cart', [ 'items' => $cart_items, 'session_id' => $this->session_id, 'total' => $total ]);
	}

}
