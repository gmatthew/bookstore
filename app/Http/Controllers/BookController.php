<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;

use Illuminate\Http\Request;

class BookController extends Controller {

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$book = App\Book::find($id);
		$authors = $book->authors()->get();

		return view('book', ['book' => $book, 'authors' => $authors]);
	}

	

}
