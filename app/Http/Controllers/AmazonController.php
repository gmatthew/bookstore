<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ApaiIO\Configuration\GenericConfiguration;
use ApaiIO\Operations\Search;
use ApaiIO\ApaiIO;
use App;

class AmazonController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index (Request $request)
    {
        
        // Configuring for Amazon
        $conf = new GenericConfiguration();
        $conf->setCountry('com')
            ->setAccessKey('AKIAJKBTFYVTADOOVKAA')
            ->setSecretKey('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')
            ->setAssociateTag('gerardco-20');
        
        $apaiIO = new ApaiIO($conf);
        
        // Obtain All Courses
        $courses = App\Course::all();
        
        foreach ($courses as $course) {
            
            // Build the search
            $search = new Search();
            $search->setCategory('Books');
            $search->setKeywords($course->title);
            $search->setResponseGroup(array(
                    'Medium',
                    'EditorialReview'
            ));
            
            $results = array();
            
            for ($i = 1; $i < 4; $i ++) {
                $search->setPage($i);
                $formattedResponse = $apaiIO->runOperation($search);
                
                $xml = simplexml_load_string($formattedResponse);
                $json = json_encode($xml);
                $array = json_decode($json, TRUE);
                
                if (isset($array['Items']['Item'])) {
                    $results = array_merge($results, $array['Items']['Item']);
                }
            }
            
            // Go through Results
            foreach ($results as $i) {
                
                if ($i['ItemAttributes']['ProductGroup'] == 'eBooks')
                    continue;
                if (! isset($i['ItemAttributes']['ListPrice']))
                    continue;
                if (! isset($i['EditorialReviews']['EditorialReview']['Content']))
                    continue;
                if (! isset($i['ItemAttributes']['Author']))
                    continue;
                if (! isset($i['LargeImage']['URL']))
                    continue;
                if (! isset($i['ItemAttributes']['ISBN']))
                    continue;
                if (! isset($i['ItemAttributes']['Label']))
                    continue;
                if (! isset($i['ItemAttributes']['Title']))
                    continue;
                    
                    // Add to Database
                $this->add_to_database($i, $course);
            }
            
            flush();
        }
    }

    public function add_to_database ($i, $course)
    {
        $author_name = (is_array($i['ItemAttributes']['Author'])) ? $i['ItemAttributes']['Author'][0] : $i['ItemAttributes']['Author'];
        $price = $i['ItemAttributes']['ListPrice']['Amount'] / 100;
        $publisher = $i['ItemAttributes']['Label'];
        $description = $i['EditorialReviews']['EditorialReview']['Content'];
        $title = $i['ItemAttributes']['Title'];
        $isbn = $i['ItemAttributes']['ISBN'];
        $cover_url = $i['LargeImage']['URL'];
        
        // Insert Author
        try {
            $author = $this->get_or_create_author($author_name);
            
            $book = App\Book::create([
                    'isbn' => $isbn,
                    'title' => $title,
                    'description' => $description,
                    'price' => number_format($price, 2),
                    'publisher' => $publisher,
                    'cover_url' => $cover_url
            ]);
            
            // Insert Book Author
            $book_author = App\Book_Author::create([
                    'book_id' => $book->id,
                    'author_id' => $author->id
            ]);
            
            // Add Books to Course
            App\Course_Book::create([
                    'book_id' => $book->id,
                    'course_id' => $course->id
            ]);
            
            echo "Added - $course->title - " . $title . '<br>';
        } catch (Illuminate\Database\QueryException $e) {
            
        } catch (\PDOException $ex) {
            
        }
    }

    private function get_or_create_author ($name)
    {
        $author = App\Author::where('name', 'LIKE', $name)->get();
        
        if (! isset($author->id)) {
            
            // Create Author
            $author = App\Author::create([
                    'name' => $name
            ]);
        }
        
        return $author;
    }

    public function list_books (Request $request)
    {
        
        // Obtain value from query string
        $q = $request->input('q');
        $qstr = "%$q%";
        
        $db = new \PDO('mysql:host=localhost;dbname=bookstore;charset=utf8', 'root', '');
        
        // Query
        $stmt = $db->prepare("SELECT * FROM Book WHERE title LIKE :q");
        $stmt->bindParam(":q", $qstr);
        $stmt->execute();
        
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
        echo '<table cellpadding=5 cellspacing=5 width=100%>';
        foreach ($results as $i) {
            
            echo '<tr>';
            echo '<td>' . $i['id'] . '</td>';
            echo '<td>' . $i['isbn'] . '</td>';
            echo '<td>' . $i['title'] . '</td>';
            echo '<td>' . $i['publisher'] . '</td>';
            echo '<td>' . $i['price'] . '</td>';
            echo '<td>' . $i['description'] . '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}
