<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App;
use Session;

abstract class Controller extends BaseController
{
    
    use DispatchesCommands, ValidatesRequests;

    protected $session_id;

    protected $user;

    protected $courses;

    function __construct ()
    {
        $this->session_id = $this->session_with_name('session_id');
        $this->user = \Auth::user();
        
        $this->courses = App\Course::all()->random(7);
        
        \View::share('session_id', $this->session_id);
        \View::share('user', $this->user);
        \View::share('courses', $this->courses);
    }

    protected function session_with_name ($key)
    {
        $session_id = Session::get($key);
        if (! $session_id) {
            $session_id = md5('RandomSessionId-' . rand(1, 10000000000000000) . 'aiuahdfuhsadkfjagiu');
            Session::put($key, $session_id);
        }
        
        return $session_id;
    }

    protected function getCartData ()
    {
        // Obtain Cart Items
        $cart_items = App\CartItem::where('session_id', 'like', $this->session_id)->get();
        
        // Calculate Final Total
        $total = 0;
        foreach ($cart_items as $item) {
            $total += (floatval($item->book->price) * floatval($item->quantity));
        }
        
        return [
                $cart_items,
                $total
        ];
    }

    protected function getStates ()
    {
        return [
                'AL' => "Alabama",
                'AK' => "Alaska",
                'AZ' => "Arizona",
                'AR' => "Arkansas",
                'CA' => "California",
                'CO' => "Colorado",
                'CT' => "Connecticut",
                'DE' => "Delaware",
                'DC' => "District Of Columbia",
                'FL' => "Florida",
                'GA' => "Georgia",
                'HI' => "Hawaii",
                'ID' => "Idaho",
                'IL' => "Illinois",
                'IN' => "Indiana",
                'IA' => "Iowa",
                'KS' => "Kansas",
                'KY' => "Kentucky",
                'LA' => "Louisiana",
                'ME' => "Maine",
                'MD' => "Maryland",
                'MA' => "Massachusetts",
                'MI' => "Michigan",
                'MN' => "Minnesota",
                'MS' => "Mississippi",
                'MO' => "Missouri",
                'MT' => "Montana",
                'NE' => "Nebraska",
                'NV' => "Nevada",
                'NH' => "New Hampshire",
                'NJ' => "New Jersey",
                'NM' => "New Mexico",
                'NY' => "New York",
                'NC' => "North Carolina",
                'ND' => "North Dakota",
                'OH' => "Ohio",
                'OK' => "Oklahoma",
                'OR' => "Oregon",
                'PA' => "Pennsylvania",
                'RI' => "Rhode Island",
                'SC' => "South Carolina",
                'SD' => "South Dakota",
                'TN' => "Tennessee",
                'TX' => "Texas",
                'UT' => "Utah",
                'VT' => "Vermont",
                'VA' => "Virginia",
                'WA' => "Washington",
                'WV' => "West Virginia",
                'WI' => "Wisconsin",
                'WY' => "Wyoming"
        ];
    }
}
