<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App;

class SearchController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $q = $request->input('q');
	    
		$results = \DB::table('Book')
		              ->join('Book_Author', 'Book.id', '=','Book_Author.book_id')
		              ->join('Author','Book_Author.author_id', '=', 'Author.id')
		              ->join('Course_Book','Course_Book.book_id', '=', 'Book.id')
		              ->join('Course', 'Course_Book.course_id', '=', 'Course.id')
		              ->where('Book.title', 'LIKE', "%$q%")
		              ->orWhere('Book.isbn', '=', $q)
		              ->orWhere('Author.name', 'LIKE', "%$q%")
		              ->orWhere('Course.code', '=', $q)
		              ->orWhere('Course.title', 'LIKE', "%$q%")
		              ->select('Book.*')
		              ->get();
		
		return view('grid', [ 'results' => $results ] );
	}

	
}
