<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use App;

class AccountController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index ()
    {
        $credit_cart_types = App\Credit_Card_Type::all()->sortBy('name');
        
        $billingAddress = $this->user->billingAddress()
            ->get()
            ->first();
        $shippingAddress = $this->user->shippingAddress()
            ->get()
            ->first();
        $creditCard = $this->user->creditCard()
            ->get()
            ->first();
        
        return view('account', [
                'billing_address' => $billingAddress ? $billingAddress : new App\Address(),
                'shipping_address' => $shippingAddress ? $shippingAddress : new App\Address(),
                'credit_card' => $creditCard ? $creditCard : new App\Credit_Card(),
                'credit_card_types' => $credit_cart_types,
                'states' => $this->getStates()
        ]);
    }

    public function update (Request $request)
    {
        $validator = Validator::make($request->all(), 
                [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'billing_address' => 'required',
                        'billing_city' => 'required',
                        'billing_state' => 'required',
                        'billing_zip' => 'required',
                        'shipping_address' => 'required',
                        'shipping_city' => 'required',
                        'shipping_state' => 'required',
                        'shipping_zip' => 'required',
                        'cc_name' => 'required',
                        'cc_type' => 'required',
                        'cc_number' => 'required',
                        'cc_month' => 'required',
                        'cc_year' => 'required',
                        'cc_verification' => 'required'
                ]);
        
        if ($validator->fails()) {
            return redirect('account')->withErrors($validator->errors());
        }
        
        $this->_update_user_info($request);
        $this->_update_billing($request);
        $this->_update_shipping($request);
        $this->_update_credit_card($request);
        
        return redirect('account');
    }

    private function _update_billing (Request $request)
    {
        $billing_data = [
                'street_1' => $request->input('billing_address'),
                'city' => $request->input('billing_city'),
                'state' => $request->input('billing_state'),
                'zip_code' => $request->input('billing_zip')
        ];
        
        // Create or Update Billing
        if ($billingAddress = $this->user->billingAddress()
            ->get()
            ->first()) {
            $billingAddress->update($billing_data);
        } else {
            $this->user->setBillingAddress($billing_data);
        }
    }

    private function _update_shipping (Request $request)
    {
        $shipping_data = [
                'street_1' => $request->input('shipping_address'),
                'city' => $request->input('shipping_city'),
                'state' => $request->input('shipping_state'),
                'zip_code' => $request->input('shipping_zip')
        ];
        
        // Create or Update Shipping
        if ($shippingAddress = $this->user->shippingAddress()
            ->get()
            ->first()) {
            $shippingAddress->update($shipping_data);
        } else {
            $this->user->setShippingAddress($shipping_data);
        }
    }

    private function _update_user_info (Request $request)
    {
        $user_data = [
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name')
        ];
        
        // Update User Infor
        $this->user->update($user_data);
    }

    private function _update_credit_card (Request $request)
    {
        $cc_data = [
                'type_id' => $request->input('cc_type'),
                'name' => $request->input('cc_name'),
                'number' => $request->input('cc_number'),
                'expiration_year' => $request->input('cc_year'),
                'expiration_month' => $request->input('cc_month'),
                'security_code' => $request->input('cc_verification')
        ];
        
        // Create or Update Shipping
        if ($creditCard = $this->user->creditCard()
            ->get()
            ->first()) {
            $creditCard->update($cc_data);
        } else {
            $this->user->setCreditCard($cc_data);
        }
    }
}
