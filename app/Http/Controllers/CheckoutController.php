<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Session;
use App;

class CheckoutController extends Controller
{

    public function index (Request $request)
    {
        list ($cart_items, $cart_total) = $this->getCartData();
        
        if (count($cart_items) == 0) {
            return redirect('cart');
        }
        
        $credit_cart_types = App\Credit_Card_Type::all()->sortBy('name');
        $billingAddress = $this->user->billingAddress()
            ->get()
            ->first();
        $shippingAddress = $this->user->shippingAddress()
            ->get()
            ->first();
        $creditCard = $this->user->creditCard()
            ->get()
            ->first();
        return view('checkout', 
                [
                        'cart_items' => $cart_items,
                        'cart_total' => $cart_total,
                        'credit_card_types' => $credit_cart_types,
                        'states' => $this->getStates(),
                        'billing_address' => $billingAddress ? $billingAddress : new App\Address(),
                        'shipping_address' => $shippingAddress ? $shippingAddress : new App\Address(),
                        'credit_card' => $creditCard ? $creditCard : new App\Credit_Card()
                ]);
    }

    public function order (Request $request)
    {
        $validator = Validator::make($request->all(), 
                [
                        'billing_first_name' => 'required',
                        'billing_last_name' => 'required',
                        'billing_address' => 'required',
                        'billing_city' => 'required',
                        'billing_state' => 'required',
                        'billing_zip' => 'required',
                        'shipping_first_name' => 'required',
                        'shipping_last_name' => 'required',
                        'shipping_address' => 'required',
                        'shipping_city' => 'required',
                        'shipping_state' => 'required',
                        'shipping_zip' => 'required',
                        'cc_name' => 'required',
                        'cc_type' => 'required',
                        'cc_number' => 'required',
                        'cc_month' => 'required',
                        'cc_year' => 'required',
                        'cc_verification' => 'required'
                ]);
        
        if ($validator->fails()) {
            return redirect('checkout')->withErrors($validator->errors());
        }
        
        $shipping_data = [
                'street_1' => $request->input('shipping_address'),
                'city' => $request->input('shipping_city'),
                'state' => $request->input('shipping_state'),
                'zip_code' => $request->input('shipping_zip')
        ];
        
        $billing_data = [
                'street_1' => $request->input('billing_address'),
                'city' => $request->input('billing_city'),
                'state' => $request->input('billing_state'),
                'zip_code' => $request->input('billing_zip')
        ];
        
        $payment_data = [
                'credit_card_type_id' => $request->input('cc_type'),
                'credit_card_name' => $request->input('cc_name'),
                'credit_card_number' => $request->input('cc_number'),
                'credit_card_expiration_year' => $request->input('cc_year'),
                'credit_card_expiration_month' => $request->input('cc_month'),
                'credit_card_security_code' => $request->input('cc_verification')
        ];
        
        // If user does not have a billing address on file, add the submitted
        // address
        // as their billing address
        if (! $this->user->billingAddress()) {
            $this->user->setBillingAddress($billing_data);
        }
        
        // If user does not have a shipping address on file, add the submitted
        // address
        // as their shipping addres
        if (! $this->user->shippingAddress()) {
            $this->user->setShippingAddress($shipping_data);
        }
        
        return $this->createOrder($payment_data, $shipping_data, $billing_data);
    }

    public function thankyou (Request $request)
    {
        $order_id = $request->input('order');
        return view('thankyou', [
                'order_id' => $order_id
        ]);
    }

    private function createOrder ($payment_data, $shipping_data, $billing_data)
    {
        list ($cart_items, $cart_total) = $this->getCartData();
        
        if (count($cart_items) > 0) {
            // Create the order
            $order = new App\Order();
            $order->customer_id = $this->user->id;
            $order->shipping = 25.00;
            $order->total = $cart_total;
            $order->shipping_street_1 = $shipping_data['street_1'];
            $order->shipping_city = $shipping_data['city'];
            $order->shipping_state = $shipping_data['state'];
            $order->shipping_zip_code = $shipping_data['zip_code'];
            $saveCount = $order->saveWithItems($cart_items);
            
            // If we were not able to successfully save then delete the order
            if ($saveCount != count($cart_items)) {
                $order->delete();
                return redirect()->back();
            }
            
            // Add Payment
            $payment = new App\Payment();
            $payment->order_id = $order->id;
            $payment->credit_card_type_id = $payment_data['credit_card_type_id'];
            $payment->credit_card_name = $payment_data['credit_card_name'];
            $payment->credit_card_number = $payment_data['credit_card_number'];
            $payment->credit_card_expiration_year = $payment_data['credit_card_expiration_year'];
            $payment->credit_card_expiration_month = $payment_data['credit_card_expiration_month'];
            $payment->credit_card_security_code = $payment_data['credit_card_security_code'];
            $payment->billing_street_1 = $billing_data['street_1'];
            $payment->billing_city = $billing_data['city'];
            $payment->billing_state = $billing_data['state'];
            $payment->billing_zip_code = $billing_data['zip_code'];
            $payment->payment_total = $order->shipping + $order->total;
            $payment->save();
            
            // Empty Cart
            foreach ($cart_items as $item) {
                $item->delete();
            }
            
            return redirect('checkout/thankyou?order=' . $order->id);
        }
        
        return redirect('cart');
    }
}
