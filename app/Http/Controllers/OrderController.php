<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index ()
    {
        $orders = $this->user->orders()->get();
        
        return view('order', [
                'orders' => $orders
        ]);
    }

    public function details ($id)
    {
        $order = Order::find($id);
        
        if (!$order) {
            return redirect('order');
        }
        
        $books = $order->books();
        
        return view('order_details', [
                'order' => $order,
                'books' => $books
        ]);
    }
}
