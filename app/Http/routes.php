<?php

/*
 * |--------------------------------------------------------------------------
 * | Application Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register all of the routes for an application.
 * | It's a breeze. Simply tell Laravel the URIs it should respond to
 * | and give it the controller to call when that URI is requested.
 * |
 */
Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::group([
        'middleware' => 'auth'
], function  ()
{
    Route::get('account', 'AccountController@index');
    Route::post('account/update', 'AccountController@update');
    Route::get('checkout', 'CheckoutController@index');
    Route::post('checkout/order', 'CheckoutController@order');
    Route::get('checkout/thankyou', 'CheckoutController@thankyou');
    Route::get('order', 'OrderController@index');
    Route::get('order/{id}', 'OrderController@details');
});

Route::get('amazon', 'AmazonController@index');
Route::get('amazon/list', 'AmazonController@list_books');
Route::get('book/{id}', 'BookController@show');
Route::post('cart/add', 'CartController@add');
Route::post('cart/delete', 'CartController@delete');
Route::post('cart/update', 'CartController@update');
Route::get('cart/summary', 'CartController@summary');
Route::get('cart', 'CartController@show');
Route::get('search', 'SearchController@index');

Route::get('contact', function  ()
{
    return view('contact');
});

Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController'
]);
