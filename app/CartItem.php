<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model {

	protected $table = 'CartItem';

	public function book() {

		return $this->hasOne('App\Book',  'id', 'book_id');
	}
}