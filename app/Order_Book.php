<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Order_Book extends Model
{

    protected $table = 'Order_Book';

    public function book ()
    {
        return $this->hasOne('App\Book', 'book_id');
    }
}
