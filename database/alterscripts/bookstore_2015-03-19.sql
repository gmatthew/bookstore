# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.41-0ubuntu0.14.04.1)
# Database: bookstore
# Generation Time: 2015-03-19 07:56:10 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Address`;

CREATE TABLE `Address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_type_id` int(11) NOT NULL,
  `street_1` varchar(255) NOT NULL,
  `street_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `address_type_id` (`address_type_id`,`customer_id`),
  KEY `address_type_id_idxfk` (`address_type_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `Address_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `Customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Address_ibfk_1` FOREIGN KEY (`address_type_id`) REFERENCES `Address_Type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Address_Type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Address_Type`;

CREATE TABLE `Address_Type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Address_Type` WRITE;
/*!40000 ALTER TABLE `Address_Type` DISABLE KEYS */;

INSERT INTO `Address_Type` (`id`, `name`)
VALUES
	(1,'Shipping'),
	(2,'Billing');

/*!40000 ALTER TABLE `Address_Type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Author
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Author`;

CREATE TABLE `Author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Book
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Book`;

CREATE TABLE `Book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `price` float NOT NULL,
  `publisher` varchar(255) NOT NULL DEFAULT '',
  `on_sale` int(11) NOT NULL DEFAULT '0',
  `cover_url` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isbn` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Book_Author
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Book_Author`;

CREATE TABLE `Book_Author` (
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  UNIQUE KEY `book_id` (`book_id`,`author_id`),
  KEY `author_id` (`author_id`),
  CONSTRAINT `Book_Author_ibfk_3` FOREIGN KEY (`book_id`) REFERENCES `Book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Book_Author_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `Author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table CartItem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `CartItem`;

CREATE TABLE `CartItem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) NOT NULL DEFAULT '',
  `book_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`,`book_id`),
  KEY `book_id_idxfk_2` (`book_id`),
  CONSTRAINT `CartItem_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `Book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Course
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Course`;

CREATE TABLE `Course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` char(4) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Course` WRITE;
/*!40000 ALTER TABLE `Course` DISABLE KEYS */;

INSERT INTO `Course` (`id`, `code`, `title`, `created_at`, `updated_at`)
VALUES
	(1,'2343','Compiler Construction','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(2,'6360','Database Design','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(3,'1337','Computer Science','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(4,'2305','Discrete Mathematics','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(5,'3440','Computer Architecture','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(6,'2311','Introduction to Digital System','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(7,'3302','Signals and Systems','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(8,'3320','Digital Circuits','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(9,'3350','Communication Systems','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(10,'1111','General Chemistry','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(11,'3361','Biochemistry I','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(12,'6317','Industrial Chemistry','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(13,'1318','Human Genetics','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(14,'3388','Honey Bee Biology','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(15,'4310','Cellular Micorbiology','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(16,'4341','Genomics','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(17,'2310','Statics','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(18,'2330','Dynamics','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(19,'3305','Computer Aided Design','2015-03-01 03:51:13','2015-03-01 03:51:13'),
	(20,'3310','Heat Transfer','2015-03-01 03:51:13','2015-03-01 03:51:13');

/*!40000 ALTER TABLE `Course` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Course_Book
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Course_Book`;

CREATE TABLE `Course_Book` (
  `course_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  KEY `course_id_idxfk` (`course_id`),
  KEY `book_id_idxfk_1` (`book_id`),
  CONSTRAINT `Course_Book_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `Book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Course_Book_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `Course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Credit_Card
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Credit_Card`;

CREATE TABLE `Credit_Card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` bigint(16) NOT NULL,
  `expiration_year` int(11) NOT NULL,
  `expiration_month` int(11) NOT NULL,
  `security_code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_customer` (`customer_id`),
  KEY `credit_card_type_id_idxfk` (`type_id`),
  CONSTRAINT `Credit_Card_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `Customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Credit_Card_Type_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `Credit_Card_Type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Credit_Card_Type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Credit_Card_Type`;

CREATE TABLE `Credit_Card_Type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Credit_Card_Type` WRITE;
/*!40000 ALTER TABLE `Credit_Card_Type` DISABLE KEYS */;

INSERT INTO `Credit_Card_Type` (`id`, `name`)
VALUES
	(1,'Amex'),
	(2,'Visa'),
	(3,'Comet Card');

/*!40000 ALTER TABLE `Credit_Card_Type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Customer`;

CREATE TABLE `Customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone_number` varchar(255) DEFAULT NULL,
  `password` varchar(60) NOT NULL DEFAULT '',
  `remember_token` varchar(100) DEFAULT NULL,
  `billing_address_id` int(11) DEFAULT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `credit_card_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_address` (`email`),
  KEY `credit_card_id` (`credit_card_id`),
  KEY `billing_address_id` (`billing_address_id`),
  KEY `shipping_address_id` (`shipping_address_id`),
  CONSTRAINT `Customer_ibfk_3` FOREIGN KEY (`shipping_address_id`) REFERENCES `Address` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `Customer_ibfk_1` FOREIGN KEY (`credit_card_id`) REFERENCES `Credit_Card` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `Customer_ibfk_2` FOREIGN KEY (`billing_address_id`) REFERENCES `Address` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Order`;

CREATE TABLE `Order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipping` float NOT NULL,
  `total` float NOT NULL,
  `shipping_street_1` varchar(255) NOT NULL DEFAULT '',
  `shipping_street_2` varchar(255) DEFAULT '',
  `shipping_city` varchar(255) NOT NULL DEFAULT '',
  `shipping_state` varchar(255) NOT NULL DEFAULT '',
  `shipping_zip_code` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id_idxfk_2` (`customer_id`),
  CONSTRAINT `Order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `Customer` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Order_Book
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Order_Book`;

CREATE TABLE `Order_Book` (
  `order_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  KEY `order_id_idxfk` (`order_id`),
  KEY `book_id_idxfk_3` (`book_id`),
  CONSTRAINT `Order_Book_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `Book` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `Order_Book_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `Order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table Payment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Payment`;

CREATE TABLE `Payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_total` int(11) NOT NULL,
  `credit_card_type_id` int(11) NOT NULL,
  `credit_card_name` varchar(255) NOT NULL,
  `credit_card_number` int(16) NOT NULL,
  `credit_card_expiration_year` int(11) NOT NULL,
  `credit_card_expiration_month` int(11) NOT NULL,
  `credit_card_security_code` int(11) NOT NULL,
  `billing_street_1` varchar(255) NOT NULL DEFAULT '',
  `billing_street_2` varchar(255) DEFAULT '',
  `billing_city` varchar(255) NOT NULL DEFAULT '',
  `billing_state` varchar(255) NOT NULL DEFAULT '',
  `billing_zip_code` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id_idxfk_1` (`order_id`),
  KEY `credit_card_type_id_idxfk` (`credit_card_type_id`),
  CONSTRAINT `Payment_ibfk_2` FOREIGN KEY (`credit_card_type_id`) REFERENCES `Credit_Card_Type` (`id`),
  CONSTRAINT `Payment_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `Order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
